## erlang-dev-container ##

This container allows the user to use it as a development environment for erlang projects.


#### How to use ? ####

```
docker container create --name erlangdev -v "$(PWD)/project:/home/project" sanketnaik/erlang-dev-container
docker container start erlangdev
```
The above command should create your containers and using the below command you can start the same. 

```
PLEASE NOTE: Prior to starting the container, please create the respective folder: $(PWD)/project
```

#### How to use  it with compose ? ####

```
version: '3'
volumes:
  erlangvol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: $HOME/project

services:
  erlangdev:
    image: sanketnaik/erlang-dev-container
    volumes:
      - erlangvol:/home/project
```
The above is a sample compose file. 

```
PLEASE NOTE: Prior to starting the containers, please create the respective folder: $HOME/project
```

#### Using for Developoment with vscode ####

Once the stack/container is up, then you can follow the instructions [here](https://code.visualstudio.com/docs/remote/containers#_attaching-to-running-containers) to attach Visual Studio Code and use this container as a development environment.
